// set up SVG for D3
var width  = 1500,
    height = 1200,
    colors = d3.scale.category10();

var svg = d3.select('div#graph')
  .append('svg')
  .attr('oncontextmenu', 'return false;')
  .attr('width', width)
  .attr('height', height);

// set up initial nodes and links
//  - nodes are known by 'id', not by index in array.
//  - reflexive edges are indicated on the node (as a bold black circle).
//  - links are always source < target; edge directions are set by 'left' and 'right'.
/*
var nodes = [
    {id: 0, reflexive: false},
    {id: 1, reflexive: true },
    {id: 2, reflexive: false}
  ],
  lastNodeId = 2,
  links = [
    {source: nodes[0], target: nodes[1], left: false, right: true },
    {source: nodes[0], target: nodes[2], left: false, right: true }
  ];
*/

var gph = {};
var force;

function setGraph(graph){
   gph = graph;

   if (force){
     force.nodes(gph.nodes)
        .links(gph.links);
   } else {
     force = d3.layout.force()
       .nodes(gph.nodes)
       .links(gph.links)
       .size([width, height])
       .linkDistance(150)
       .charge(-500)
       .on('tick', tick);
   }

   restart();
}

function setEmptyGraph(){
  setGraph({
        lastNodeId:0,
        nodes:[],
        links:[],
        nodeMap:{},
        name:'',
        detail:''
  });
}

// init D3 force layout

// define arrow markers for graph links
svg.append('svg:defs').append('svg:marker')
    .attr('id', 'end-arrow')
    .attr('viewBox', '0 -5 10 10')
    .attr('refX', 6)
    .attr('markerWidth', 3)
    .attr('markerHeight', 3)
    .attr('orient', 'auto')
  .append('svg:path')
    .attr('d', 'M0,-5L10,0L0,5')
    .attr('fill', '#000');

svg.append('svg:defs').append('svg:marker')
    .attr('id', 'start-arrow')
    .attr('viewBox', '0 -5 10 10')
    .attr('refX', 4)
    .attr('markerWidth', 3)
    .attr('markerHeight', 3)
    .attr('orient', 'auto')
  .append('svg:path')
    .attr('d', 'M10,-5L0,0L10,5')
    .attr('fill', '#000');

// line displayed when dragging new nodes
var drag_line = svg.append('svg:path')
  .attr('class', 'link dragline hidden')
  .attr('d', 'M0,0L0,0');

// handles to link and node element groups
var linkLabel = svg.append('svg:g').selectAll('g.link-label'),
    path = svg.append('svg:g').selectAll('path'),
    circle = svg.append('svg:g').selectAll('g.cicle');


// mouse event vars
var selected_node = null,
    selected_link = null,
    mousedown_link = null,
    mousedown_node = null,
    mouseup_node = null;

function resetMouseVars() {
  mousedown_node = null;
  mouseup_node = null;
  mousedown_link = null;
}

var labelLinkPosition = function(d) {
		if (d && d.target && d.target.x){
			var dx = (d.target.x - d.source.x),
				dy = (d.target.y - d.source.y);
			var dr = Math.sqrt(dx * dx + dy * dy);
			var offset = (1 - (1 / dr)) / 2;
			var deg = 180 / Math.PI * Math.atan2(dy, dx);

			var offRad = ((deg+90)*Math.PI)/180;

			var offX = Math.cos(offRad)*15;
			var offY = Math.sin(offRad)*15;

			var x = (d.source.x + dx * offset) + offX;
			var y = (d.source.y + dy * offset) + offY;

      //x = isNaN(x)?0:x;
      //y = isNaN(y)?0:y;


			return "translate(" + x + ", " + y + ") rotate(" + deg + ")";
		} else {
			return "";
		}
};
/*
Formula para calcular ponto na borda de um quadrado
rcx = center x rectangle,
rcy = center y rectangle,
rw = width rectangle,
rh = height rectangle,
rr = rotation in radian from the rectangle (around it's center point)

function toRectObjectFromCenter(rcx, rcy, rw, rh, rr){
    var a = {
        x: rcx+(Math.sin((rr-degToRad(90))+Math.asin(rh/(Math.sqrt(rh*rh+rw*rw)))) * (Math.sqrt(rh*rh+rw*rw)/2)),
        y: rcy-(Math.cos((rr-degToRad(90))+Math.asin(rh/(Math.sqrt(rh*rh+rw*rw)))) * (Math.sqrt(rh*rh+rw*rw)/2))
    };
    var b = {
        x: a.x+Math.cos(rr)*rw,
        y: a.y+Math.sin(rr)*rw
    };
    var c = {
        x: b.x+Math.cos(degToRad(radToDeg(rr)+90))*rh,
        y: b.y+Math.sin(degToRad(radToDeg(rr)+90))*rh
    };
    var d = {
        x: a.x+Math.cos(degToRad(radToDeg(rr)+90))*rh,
        y: a.y+Math.sin(degToRad(radToDeg(rr)+90))*rh
    };
    return {a:a,b:b,c:c,d:d};
}
*/

// update force layout (called automatically each iteration)
function tick() {
  // draw directed edges with proper padding from node centers
  path.attr('d', function(d) {
    var deltaX = d.target.x - d.source.x,
        deltaY = d.target.y - d.source.y,
        dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
        normX = deltaX / dist,
        normY = deltaY / dist,
        sourcePadding = d.left ? 32 : 30,
        targetPadding = d.right ? 32 : 30,
        sourceX = d.source.x + (sourcePadding * normX),
        sourceY = d.source.y + (sourcePadding * normY),
        targetX = d.target.x - (targetPadding * normX),
        targetY = d.target.y - (targetPadding * normY);

        //sourceX = isNaN(sourceX)?0:sourceX;
        //sourceY = isNaN(sourceY)?0:sourceY;
        //targetX = isNaN(targetX)?0:targetX;
        //targetX = isNaN(targetY)?0:targetY;

    return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
  });

  circle.attr('transform', function(d) {
    //d.x = isNaN(d.x)?0:d.x;
    //d.y = isNaN(d.y)?0:d.y;

    return 'translate(' + d.x + ',' + d.y + ')';
  });

  linkLabel.attr("transform", labelLinkPosition);

}


// update graph (called when needed)
function restart() {

	  // add new text-links
	  linkLabel = linkLabel.data(gph.links);

	  linkLabel.enter()
	    .append("svg:g")
	    .attr("class", "link-label")
	    .attr("transform", labelLinkPosition)
	    .append("svg:text")
	    .attr("text-anchor", "middle")
	    .text(function(d) {
			if (d && d.target){
				return d.source.name + ' -> ' + d.target.name;
			} else {
				return '';
			}
		});


	  // remove old links
	  linkLabel.exit().remove();

  // path (link) group
  path = path.data(gph.links);

  // update existing links
  path.classed('selected', function(d) { return d === selected_link; })
    .style('marker-start', function(d) { return d.left ? 'url(#start-arrow)' : ''; })
    .style('marker-end', function(d) { return d.right ? 'url(#end-arrow)' : ''; });



  // add new links
  path.enter().append('svg:path')
    .attr('class', 'link')
    .classed('selected', function(d) { return d === selected_link; })
    .style('marker-start', function(d) { return d.left ? 'url(#start-arrow)' : ''; })
    .style('marker-end', function(d) { return d.right ? 'url(#end-arrow)' : ''; })
    .on('mousedown', function(d) {
      if(d3.event.ctrlKey) return;

      // select link
      mousedown_link = d;
      if(mousedown_link === selected_link) selected_link = null;
      else selected_link = mousedown_link;
      selected_node = null;
      restart();
    });

  // remove old links
  path.exit().remove();


  // circle (node) group
  // NB: the function arg is crucial here! nodes are known by id, not by index!
  circle = circle.data(gph.nodes, function(d) { return d.id; });

  // update existing nodes (reflexive & selected visual states)
  circle.selectAll('circle')
    .style('fill', function(d) { return (d === selected_node) ? d3.rgb(colors(d.id)).brighter().toString() : colors(d.id); })
    .classed('reflexive', function(d) { return d.reflexive; });

  // add new nodes
  var g = circle.enter().append('svg:g').attr('classe','cicle');

//  <rect x="20" y="10" width="200" height="100" fill="none" stroke="blue" stroke-width="5" rx="20" />
//  g.append('svg:circle')
//    .attr('class', 'node')
 //   .attr('r', 30)
  g.append('svg:rect')
    .attr('class', 'node')
    .attr('x', -40)
    .attr('y', -10)
    .attr('width', 80)
    .attr('height', 20)
    .attr('rx', 8)
    .style('fill', function(d) { return (d === selected_node) ? d3.rgb(colors(d.id)).brighter().toString() : colors(d.id); })
    .style('stroke', function(d) { return d3.rgb(colors(d.id)).darker().toString(); })
    .classed('reflexive', function(d) { return d.reflexive; })
    .on('mouseover', function(d) {
      if(!mousedown_node || d === mousedown_node) return;
      // enlarge target node
      d3.select(this).attr('transform', 'scale(1.1)');
    })
    .on('mouseout', function(d) {
      if(!mousedown_node || d === mousedown_node) return;
      // unenlarge target node
      d3.select(this).attr('transform', '');
    })
    .on('mousedown', function(d) {
      if(d3.event.ctrlKey) return;

      // select node
      mousedown_node = d;
      if(mousedown_node === selected_node) selected_node = null;
      else selected_node = mousedown_node;
      selected_link = null;

      // reposition drag line
      drag_line
        .style('marker-end', 'url(#end-arrow)')
        .classed('hidden', false)
        .attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + mousedown_node.x + ',' + mousedown_node.y);

      restart();
    })
    .on('mouseup', function(d) {
      if(!mousedown_node) return;

      // needed by FF
      drag_line
        .classed('hidden', true)
        .style('marker-end', '');

      // check for drag-to-self
      mouseup_node = d;
      if(mouseup_node === mousedown_node) { resetMouseVars(); return; }

      // unenlarge target node
      d3.select(this).attr('transform', '');

      // add link to graph (update if exists)
      // NB: links are strictly source < target; arrows separately specified by booleans
      var source, target, direction;
      if(mousedown_node.id < mouseup_node.id) {
        source = mousedown_node;
        target = mouseup_node;
        direction = 'right';
      } else {
        source = mouseup_node;
        target = mousedown_node;
        direction = 'left';
      }

      var link;
      link = gph.links.filter(function(l) {
        return (l.source === source && l.target === target);
      })[0];

      if(link) {
        link[direction] = true;
      } else {
        link = {source: source, target: target, left: false, right: false};
        link[direction] = true;
        gph.links.push(link);
      }

      // select new link
      selected_link = link;
      selected_node = null;
      restart();
    });

  // show node IDs
  g.append('svg:text')
      .attr('x', 0)
      .attr('y', 4)
      .attr('class', 'id')
      .text(function(d) { return d.name; });

  // remove old nodes
  circle.exit().remove();

  // set the graph in motion
  force.start();
}

function mousedown() {
  // prevent I-bar on drag
  //d3.event.preventDefault();

  // because :active only works in WebKit?
  svg.classed('active', true);

  if(d3.event.ctrlKey || mousedown_node || mousedown_link) return;
/*
  // insert new node at point
  var point = d3.mouse(this),
      node = {id: ++lastNodeId, reflexive: false};
  node.x = point[0];
  node.y = point[1];
  nodes.push(node);

  restart();*/
}

function mousemove() {
  if(!mousedown_node) return;

  // update drag line
  drag_line.attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + d3.mouse(this)[0] + ',' + d3.mouse(this)[1]);

  restart();
}

function mouseup() {
  if(mousedown_node) {
    // hide drag line
    drag_line
      .classed('hidden', true)
      .style('marker-end', '');
  }

  // because :active only works in WebKit?
  svg.classed('active', false);

  // clear mouse event vars
  resetMouseVars();
}

function spliceLinksForNode(node) {
  var toSplice = gph.links.filter(function(l) {
    return (l.source === node || l.target === node);
  });
  toSplice.map(function(l) {
    gph.links.splice(gph.links.indexOf(l), 1);
  });
}

// only respond once per keydown
var lastKeyDown = -1;

function keydown() {
  d3.event.preventDefault();

  if(lastKeyDown !== -1) return;
  lastKeyDown = d3.event.keyCode;

  // ctrl
  if(d3.event.keyCode === 17) {
    circle.call(force.drag);
    svg.classed('ctrl', true);
  }

  if(!selected_node && !selected_link) return;
  switch(d3.event.keyCode) {
    case 8: // backspace
    case 46: // delete
      if(selected_node) {
        gph.nodes.splice(gph.nodes.indexOf(selected_node), 1);
        spliceLinksForNode(selected_node);
      } else if(selected_link) {
        gph.links.splice(gph.links.indexOf(selected_link), 1);
      }
      selected_link = null;
      selected_node = null;
      restart();
      break;
    case 66: // B
      if(selected_link) {
        // set link direction to both left and right
        selected_link.left = true;
        selected_link.right = true;
      }
      restart();
      break;
    case 76: // L
      if(selected_link) {
        // set link direction to left only
        selected_link.left = true;
        selected_link.right = false;
      }
      restart();
      break;
    case 82: // R
      if(selected_node) {
        // toggle node reflexivity
        selected_node.reflexive = !selected_node.reflexive;
      } else if(selected_link) {
        // set link direction to right only
        selected_link.left = false;
        selected_link.right = true;
      }
      restart();
      break;
  }
}

function keyup() {
  lastKeyDown = -1;

  // ctrl
  if(d3.event.keyCode === 17) {
    circle
      .on('mousedown.drag', null)
      .on('touchstart.drag', null);
    svg.classed('ctrl', false);
  }
}



function addNode(nameNode){
	if (!gph.nodeMap[nameNode]){
    node = {name: nameNode, id: ++gph.lastNodeId, reflexive: false, x:-40, y:-20};
		pos = gph.nodes.push(node);
		gph.nodeMap[nameNode] = {position:pos-1};
    return true;
	}
  return false;
}

function addLink(nameSource, nameTarget){
	sourceInfo = gph.nodeMap[nameSource];
	targetInfo = gph.nodeMap[nameTarget];
	if (sourceInfo && targetInfo){
		gph.links.push({source: gph.nodes[sourceInfo.position], target: gph.nodes[targetInfo.position], left: false, right: true });
	}
}

function removeNode(name){
	nodeInfo = gph.nodeMap[name];
	if (nodeInfo){
		pos = nodeInfo.position;
		node = gph.nodes[pos];
		spliceLinksForNode(node);
    gph.nodes.splice(pos, 1);
		gph.nodeMap[name] = undefined;
		for (x in gph.nodeMap){
			nodeInfo = gph.nodeMap[x];
			nodeInfo.position = nodeInfo.position > pos?nodeInfo.position - 1:nodeInfo.position;
		}
	}

}

function targetsIdsOf(name) {
	nodeInfo = gph.nodeMap[name];
	if (nodeInfo){
	   node = gph.nodes[nodeInfo.position];
	   return gph.links.filter(function(l) {
			return (l.source == node);
	   }).map(function(l) {
			return l.target.name;
	   });
	} else {
		return [];
	}

}


// app starts here
svg.on('mousedown', mousedown)
  .on('mousemove', mousemove)
  .on('mouseup', mouseup)//;
//d3.select('div#graph')
  .on('keydown', keydown)
  .on('keyup', keyup);
//restart();
setEmptyGraph();
